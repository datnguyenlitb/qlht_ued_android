package com.example.finalprojectandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalprojectandroid.Model.Credit;
import com.example.finalprojectandroid.R;

import java.util.List;

public class CreditAdapter  extends RecyclerView.Adapter<CreditAdapter.ViewHolder> {

    private List<Credit> data;
    private Context context;

    public CreditAdapter(Context context, List<Credit> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.credit_item, parent,false);
        return new CreditAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSubjectCode, tvGroupCode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvSubjectCode = (TextView)itemView.findViewById(R.id.tvSubjectCode);
            tvGroupCode = (TextView)itemView.findViewById(R.id.tvGroupCode);
        }
    }
}
