package com.example.finalprojectandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalprojectandroid.Model.TimeTableItem;
import com.example.finalprojectandroid.R;

import java.util.List;

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.ViewHolder> {

    List<TimeTableItem> listSubjects;
    private Context context;

    public TimeTableAdapter(List<TimeTableItem> listSubjects, Context context) {
        this.listSubjects = listSubjects;
        this.context = context;
    }


    @NonNull
    @Override
    public TimeTableAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_table_item, parent,false);
        return new TimeTableAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TimeTableItem subject = listSubjects.get(position);

        holder.tvName.setText(subject.getName());
        holder.tvRoom.setText(subject.getRoom());
        holder.tvTeacher.setText(subject.getTeacher());
        holder.tvWeek.setText(subject.getWeek());
        holder.tvDayOfWeek.setText(subject.getDayOfWeek());
        holder.tvLes.setText(subject.getLes());
    }


    @Override
    public int getItemCount() {
        return listSubjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvName, tvRoom, tvTeacher, tvWeek, tvDayOfWeek, tvLes;

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView)itemView.findViewById(R.id.tvName);
            tvRoom = (TextView)itemView.findViewById(R.id.tvRoom);
            tvTeacher = (TextView)itemView.findViewById(R.id.tvTeacher);
            tvWeek = (TextView)itemView.findViewById(R.id.tvWeek);
            tvDayOfWeek = (TextView)itemView.findViewById(R.id.tvDayOfWeek);
            tvLes = (TextView)itemView.findViewById(R.id.tvLes);
        }
    }
}
