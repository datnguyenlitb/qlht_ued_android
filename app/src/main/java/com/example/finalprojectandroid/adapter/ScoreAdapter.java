package com.example.finalprojectandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.finalprojectandroid.Model.Subject;
import com.example.finalprojectandroid.Model.TermList;
import com.example.finalprojectandroid.R;

import java.util.List;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder> {

    private List<Subject> subjectList ;
    private Context context;

    public ScoreAdapter(List<Subject> subjectList, Context context) {
        this.subjectList = subjectList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.score_item, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Subject subject = subjectList.get(position);

        //This will randomly color all circle
//        Random mRandom = new Random();
//        int color = Color.argb(255, mRandom.nextInt(256), mRandom.nextInt(256), mRandom.nextInt(256));
//        ((GradientDrawable) holder.textCircle.getBackground()).setColor(color);

        holder.tvSubjectName.setText(subject.getName());
        holder.tvAveragePoint.setText(subject.getAverage());
        holder.tvRank.setText(subject.getType());
    }

    @Override
    public int getItemCount() {
        return subjectList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvSubjectName, tvAveragePoint,tvRank;

        public ViewHolder(View itemView) {
            super(itemView);

            tvSubjectName = (TextView)itemView.findViewById(R.id.subjectName);
            tvAveragePoint = (TextView)itemView.findViewById(R.id.averagePoint);
            tvRank = (TextView)itemView.findViewById(R.id.rank);
        }
    }
}
