package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.finalprojectandroid.Model.Credit;
import com.example.finalprojectandroid.adapter.CreditAdapter;
import com.example.finalprojectandroid.adapter.TimeTableAdapter;

import java.util.ArrayList;
import java.util.List;

public class CreditPrepareActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText edtSubjectCode, edtGroupCode;
    private List<Credit> listCredit = new ArrayList<Credit>();
    RecyclerView recyclerView;
    CreditAdapter creditAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_prepare);

        edtSubjectCode = findViewById(R.id.edtSubjectCode);
        edtGroupCode = findViewById(R.id.edtGroupCode);
        recyclerView = findViewById(R.id.recyclerViewCreditList);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Đăng ký học phần");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        creditAdapter = new CreditAdapter(getApplicationContext(), listCredit);
        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(eLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(creditAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void addSubject(View view) {
        String subjectCode = edtSubjectCode.getText().toString().trim();
        String groupCode = edtGroupCode.getText().toString().trim();

        if (subjectCode.isEmpty() || groupCode.isEmpty()) {
            Toast.makeText(CreditPrepareActivity.this, "Vui lòng điền đẩy đủ dữ liệu!", Toast.LENGTH_SHORT).show();
        } else {
            listCredit.add(new Credit(subjectCode, groupCode));
            creditAdapter.notifyDataSetChanged();
            edtSubjectCode.setText("");
            edtGroupCode.setText("");

            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }

    }
}
