package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.finalprojectandroid.Model.Information;
import com.example.finalprojectandroid.Model.User;
import com.example.finalprojectandroid.api.APIService;
import com.example.finalprojectandroid.utils.RetroClient;
import com.example.finalprojectandroid.utils.SaveSharedPreference;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {


    private ProgressBar spinner;
    private Retrofit retrofit;
    private Button btnLogin;
    private EditText edtStdCode;
    private EditText edtPwd;
    private String code;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtStdCode = findViewById(R.id.edtStdCode);
        edtPwd = findViewById(R.id.edtPwd);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if (SaveSharedPreference.getLoggedStatus(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), Dashboard.class);
            startActivity(intent);
        }


        spinner = (ProgressBar) findViewById(R.id.progressBar);
        btnLogin = findViewById(R.id.btnLogin);

        spinner.setVisibility(View.GONE);

    }

    boolean validateInput(String scode, String password) {
        if (scode.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Vui lòng nhập mã sinh viên!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Vui lòng nhập mật khẩu!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void login(View view) {

        code = edtStdCode.getText().toString();
        password = edtPwd.getText().toString();

        if (validateInput(code, password)) {
            btnLogin.setEnabled(false);
            spinner.setVisibility(View.VISIBLE);

//            Toast.makeText(LoginActivity.this, code, Toast.LENGTH_SHORT).show();
//            Toast.makeText(LoginActivity.this, password, Toast.LENGTH_SHORT).show();

            APIService api = RetroClient.getApiService();

            Call<User> call = api.login(code, password);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        User user = response.body();
                        Log.i("response", user.getStudentCode());
                        btnLogin.setEnabled(true);
                        spinner.setVisibility(View.GONE);

                        SaveSharedPreference.setLoggedIn(getApplicationContext(), true);

                        Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                        intent.putExtra("studentCode", user.getStudentCode());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
//                    Log.d("RS:", t.getMessage());
                    btnLogin.setEnabled(true);
                    spinner.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, "Có gì đó không đúng, vui lòng thử lại!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
