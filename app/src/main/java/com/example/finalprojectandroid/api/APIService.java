package com.example.finalprojectandroid.api;

import com.example.finalprojectandroid.Model.Fee;
import com.example.finalprojectandroid.Model.Information;
import com.example.finalprojectandroid.Model.Term;
import com.example.finalprojectandroid.Model.TimeTable;
import com.example.finalprojectandroid.Model.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {

    @POST("/auth/login")
    @FormUrlEncoded
    Call<User> login(@Field("scode") String username,
                     @Field("password") String password);

    @POST("/score/index")
    Call<Term> getStudyResult();

    @GET("/info/index")
    Call<Information> getInfo();

    @GET("/fee/index")
    Call<Fee> getFee();

    @GET("/schedule/index")
    Call<TimeTable> getTimeTable();
}