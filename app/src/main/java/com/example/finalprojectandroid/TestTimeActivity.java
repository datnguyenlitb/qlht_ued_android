package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.view.View;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class TestTimeActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_time);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lịch thi");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void createAlarm(View view) {

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2019, 12-2, 20, 6, 00);
        Calendar endTime = Calendar.getInstance();
        endTime.set(2019, 12-1, 20, 7, 00);
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(Events.TITLE, "Chuẩn bị đi thi Pháp luật đại cương")
                .putExtra(Events.DESCRIPTION, "Cố gắng chuẩn bị thật chu đáo.")
                .putExtra(Events.EVENT_LOCATION, "454/29 Tôn Đức Thắng, Đà Nẵng, Việt Nam")
                .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
        startActivity(intent);
    }
}
