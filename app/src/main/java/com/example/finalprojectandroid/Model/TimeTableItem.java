package com.example.finalprojectandroid.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeTableItem {

    @SerializedName("dayOfWeek")
    @Expose
    private String dayOfWeek;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("les")
    @Expose
    private String les;
    @SerializedName("teacher")
    @Expose
    private String teacher;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("week")
    @Expose
    private String week;

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLes() {
        return les;
    }

    public void setLes(String les) {
        this.les = les;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }
}