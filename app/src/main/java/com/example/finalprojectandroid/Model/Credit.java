package com.example.finalprojectandroid.Model;

public class Credit {
    private String subjectCode;
    private String groupCode;

    public Credit(String subjectCode, String groupCode) {
        this.subjectCode = subjectCode;
        this.groupCode = groupCode;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }
}
