package com.example.finalprojectandroid.Model;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Term {
    @SerializedName("termlist")
    @Expose
    private List<TermList> termlist = null;

    public List<TermList> getTermlist() {
        return termlist;
    }

    public void setTermlist(List<TermList> termlist) {
        this.termlist = termlist;
    }
}
