package com.example.finalprojectandroid.Model;

import com.google.gson.annotations.SerializedName;

public final class Score {
    @SerializedName("_id")
    private String id;

    @SerializedName("token")
    private String token;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("username")
    private String username;
}