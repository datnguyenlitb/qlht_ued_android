package com.example.finalprojectandroid.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TermList {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("subjects")
    @Expose
    private List<Subject> subjects = null;

    public TermList() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        return "TermList{" +
                "subjects=" + subjects +
                '}';
    }
}
