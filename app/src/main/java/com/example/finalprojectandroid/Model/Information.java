package com.example.finalprojectandroid.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Information {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("student_code")
    @Expose
    private String studentCode;
    @SerializedName("typeOfTraining")
    @Expose
    private String typeOfTraining;
    @SerializedName("areaOfTraining")
    @Expose
    private String areaOfTraining;
    @SerializedName("numberOfParent")
    @Expose
    private String numberOfParent;

    public Information() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getTypeOfTraining() {
        return typeOfTraining;
    }

    public void setTypeOfTraining(String typeOfTraining) {
        this.typeOfTraining = typeOfTraining;
    }

    public String getAreaOfTraining() {
        return areaOfTraining;
    }

    public void setAreaOfTraining(String areaOfTraining) {
        this.areaOfTraining = areaOfTraining;
    }

    public String getNumberOfParent() {
        return numberOfParent;
    }

    public void setNumberOfParent(String numberOfParent) {
        this.numberOfParent = numberOfParent;
    }

}