package com.example.finalprojectandroid.Model;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeTable {

    @SerializedName("data")
    @Expose
    private List<TimeTableItem> timeTable = null;

    public List<TimeTableItem> getTimeTable() {
        return timeTable;
    }

    public void setData(List<TimeTableItem> timeTable) {
        this.timeTable = timeTable;
    }
}
