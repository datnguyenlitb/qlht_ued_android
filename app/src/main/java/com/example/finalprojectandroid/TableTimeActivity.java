package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.finalprojectandroid.Model.Subject;
import com.example.finalprojectandroid.Model.TermList;
import com.example.finalprojectandroid.Model.TimeTable;
import com.example.finalprojectandroid.Model.TimeTableItem;
import com.example.finalprojectandroid.adapter.ScoreAdapter;
import com.example.finalprojectandroid.adapter.TimeTableAdapter;
import com.example.finalprojectandroid.api.APIService;
import com.example.finalprojectandroid.utils.RetroClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TableTimeActivity extends AppCompatActivity {
    private Toolbar toolbar;
    ProgressBar progressBar;
    SharedPreferences mPrefs;
    Button btnRefresh;
    List<TimeTableItem> listSubjects;
    private TimeTableAdapter timeTableAdapter;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_time);

        mPrefs = getApplicationContext().getSharedPreferences("info", 0);
        recyclerView = findViewById(R.id.myRecyclerViewTT);
        progressBar = findViewById(R.id.progressBar3);
        btnRefresh = findViewById(R.id.btnRefreshTT);

        mPrefs = getPreferences(MODE_PRIVATE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Thời khóa biểu cá nhân");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (!mPrefs.contains("timetable")) {
            fetchData();
        } else {
            Gson gson = new Gson();
            String ttableJson = mPrefs.getString("timetable", "");
            listSubjects = getObjectList(ttableJson, TimeTableItem.class);


            timeTableAdapter = new TimeTableAdapter(listSubjects, getApplicationContext());
            RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(eLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(timeTableAdapter);
        }
    }

    public static <T> List<T> getObjectList(String jsonString, Class<T> cls){
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            JsonArray arry = new JsonParser().parse(jsonString).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                list.add(gson.fromJson(jsonElement, cls));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    void fetchData() {
        progressBar.setVisibility(View.VISIBLE);
        btnRefresh.setEnabled(false);

        APIService api = RetroClient.getApiService();

        Call<TimeTable> call = api.getTimeTable();

        call.enqueue(new Callback<TimeTable>() {
            @Override
            public void onResponse(Call<TimeTable> call, Response<TimeTable> response) {
                if (response.isSuccessful()) {
                    listSubjects = response.body().getTimeTable();

                    SharedPreferences.Editor prefsEditor = mPrefs.edit();
                    Gson gson = new Gson();

                    String resultJson = gson.toJson(listSubjects);
                    prefsEditor.putString("timetable", resultJson);
                    prefsEditor.commit();

                    timeTableAdapter = new TimeTableAdapter(listSubjects, getApplicationContext());
                    RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(eLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(timeTableAdapter);
                }

                btnRefresh.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<TimeTable> call, Throwable t) {
//                pDialog.dismiss();
                Toast.makeText(TableTimeActivity.this, "Có gì đó không đúng, vui lòng tải lại!", Toast.LENGTH_SHORT).show();
                btnRefresh.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void doRefresh(View view) {
        fetchData();
    }
}
