package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.finalprojectandroid.Model.Information;
import com.example.finalprojectandroid.api.APIService;
import com.example.finalprojectandroid.utils.RetroClient;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tvName, tvGender, tvStudentCode, tvID, tvBirthday, tvTypeOfTraining, tvAreaOfTraining, tvNumberParent;
    private Information information = new Information();
    SharedPreferences mPrefs;
    private ProgressBar progressBar;
    private Button btnRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        mPrefs = getApplicationContext().getSharedPreferences("info", 0);

        progressBar = findViewById(R.id.progressBar4);
        btnRefresh = findViewById(R.id.btnRefresh);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Thông tin sinh viên");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvName = findViewById(R.id.tvName);
        tvGender = findViewById(R.id.tvGender);
        tvStudentCode = findViewById(R.id.tvStudentCode);
        tvID = findViewById(R.id.tvID);
        tvBirthday = findViewById(R.id.tvBirthday);
        tvTypeOfTraining = findViewById(R.id.tvTypeOfTraining);
        tvAreaOfTraining = findViewById(R.id.tvAreaOfTraining);
        tvNumberParent = findViewById(R.id.tvNumberParent);

        Gson gson = new Gson();
        String infoJson = mPrefs.getString("infoJson", "");

        if (!mPrefs.contains("infoJson")) {
            refresh();
        } else {
            information = gson.fromJson(infoJson, Information.class);
            fillInfoToTextViews(information);
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void fillInfoToTextViews(Information information) {
        String name = information.getName(),
                gender = information.getGender(),
                studentCode = information.getStudentCode(),
                id = information.getId(),
                birthday = information.getBirthday(),
                typeOfTraining = information.getTypeOfTraining(),
                areaOfTraining = information.getAreaOfTraining(),
                numberOfParent = information.getNumberOfParent();

        tvName.setText(name);
        tvGender.setText(gender);
        tvStudentCode.setText(studentCode);
        tvID.setText(id);
        tvBirthday.setText(birthday);
        tvTypeOfTraining.setText(typeOfTraining);
        tvAreaOfTraining.setText(areaOfTraining);
        tvNumberParent.setText(numberOfParent);
    }

    void refresh() {
        btnRefresh.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);

        APIService api = RetroClient.getApiService();

        Call<Information> call = api.getInfo();

        call.enqueue(new Callback<Information>() {
            @Override
            public void onResponse(Call<Information> call, Response<Information> response) {

                btnRefresh.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    information = response.body();

                    Editor prefsEditor = mPrefs.edit();
                    Gson gson = new Gson();

                    String infoJson = gson.toJson(information);
                    prefsEditor.putString("infoJson", infoJson);
                    prefsEditor.commit();

                    fillInfoToTextViews(information);
                }
            }

            @Override
            public void onFailure(Call<Information> call, Throwable t) {
                Toast.makeText(InfoActivity.this, "Có gì đó không đúng, vui lòng tải lại!", Toast.LENGTH_SHORT).show();
                btnRefresh.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void doRefreshInfo(View view) {
        refresh();
    }
}
