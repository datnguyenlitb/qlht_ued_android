package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.finalprojectandroid.Model.Fee;
import com.example.finalprojectandroid.Model.Information;
import com.example.finalprojectandroid.api.APIService;
import com.example.finalprojectandroid.utils.RetroClient;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    SharedPreferences mPrefs;
    Fee fee;
    TextView tvTotal, tvDone;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee);

        progressBar = findViewById(R.id.progressBar2);
        tvTotal = findViewById(R.id.tvTotal);
        tvDone = findViewById(R.id.tvDone);

        mPrefs = getApplicationContext().getSharedPreferences("info", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Học phí");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Gson gson = new Gson();
        String feeJson = mPrefs.getString("fee", "");

        if (!mPrefs.contains("fee")) {
            doFetch();
        } else {
            fee = gson.fromJson(feeJson, Fee.class);
            fillDataTv(fee);
        }
    }

    private void fillDataTv(Fee fee) {
        tvTotal.setText(fee.getTotal());
        tvDone.setText(fee.getDone());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void doFetch() {
        progressBar.setVisibility(View.VISIBLE);

        APIService api = RetroClient.getApiService();

        Call<Fee> call = api.getFee();

        call.enqueue(new Callback<Fee>() {
            @Override
            public void onResponse(Call<Fee> call, Response<Fee> response) {

                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    fee = response.body();

                    SharedPreferences.Editor prefsEditor = mPrefs.edit();
                    Gson gson = new Gson();

                    String infoJson = gson.toJson(fee);
                    prefsEditor.putString("fee", infoJson);
                    prefsEditor.commit();

                    fillDataTv(fee);
                }
            }

            @Override
            public void onFailure(Call<Fee> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void doRefresh(View view) {
        doFetch();
    }
}
