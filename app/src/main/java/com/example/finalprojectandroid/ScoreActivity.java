package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.finalprojectandroid.Model.Information;
import com.example.finalprojectandroid.Model.Subject;
import com.example.finalprojectandroid.Model.Term;
import com.example.finalprojectandroid.Model.TermList;
import com.example.finalprojectandroid.Model.User;
import com.example.finalprojectandroid.adapter.ScoreAdapter;
import com.example.finalprojectandroid.api.APIService;
import com.example.finalprojectandroid.utils.RetroClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ScoreActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private ScoreAdapter scoreAdapter;
    private RecyclerView recyclerView;
    private Retrofit retrofit;
    private List<TermList> termList;
    SharedPreferences mPrefs;
    Spinner spTerm;
    int pos = 0;

    TermList resultOfTerm;
    Button btnSendToHome, btnShare, btnRefresh;
    ProgressBar processBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        mPrefs = getApplicationContext().getSharedPreferences("info", 0);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnSendToHome = findViewById(R.id.btnSendToHome);
        btnShare = findViewById(R.id.btnShare);
        processBar = findViewById(R.id.progressBar);
        btnRefresh = findViewById(R.id.btnRefresh);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Kết quả học tập");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        spTerm = (Spinner) findViewById(R.id.spTerm);
        recyclerView = (RecyclerView) findViewById(R.id.myRecyclerView);

        if (!mPrefs.contains("studyResult")) {
            fetchData();
        } else {
            Gson gson = new Gson();
            String resultJson = mPrefs.getString("studyResult", "");

            termList = getObjectList(resultJson, TermList.class);
//            gson.fromJson(resultJson, List.class);
            fillData(termList);
        }
    }

    public static <T> List<T> getObjectList(String jsonString,Class<T> cls){
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            JsonArray arry = new JsonParser().parse(jsonString).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                list.add(gson.fromJson(jsonElement, cls));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    void fillData(List<TermList> termList) {
        List<String> termNameList = new ArrayList<String>();

        for (int i = 0; i < termList.size(); i++) {
            termNameList.add(termList.get(i).getName());
        }

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, termNameList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spTerm.setAdapter(spinnerAdapter);
        spTerm.setOnItemSelectedListener(ScoreActivity.this);
    }

    void fetchData() {
        btnRefresh.setEnabled(false);
        processBar.setVisibility(View.VISIBLE);
        APIService api = RetroClient.getApiService();

        Call<Term> call = api.getStudyResult();

        call.enqueue(new Callback<Term>() {
            @Override
            public void onResponse(Call<Term> call, Response<Term> response) {

                if (response.isSuccessful()) {
                    termList = response.body().getTermlist();

                    SharedPreferences.Editor prefsEditor = mPrefs.edit();
                    Gson gson = new Gson();

                    String resultJson = gson.toJson(termList);
                    prefsEditor.putString("studyResult", resultJson);
                    prefsEditor.commit();

                    fillData(termList);
                }

                btnRefresh.setEnabled(true);
                processBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Term> call, Throwable t) {
//                pDialog.dismiss();
                Toast.makeText(ScoreActivity.this, "Có gì đó không đúng, vui lòng tải lại!", Toast.LENGTH_SHORT).show();
                btnRefresh.setEnabled(true);
                processBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        pos = position;
        resultOfTerm = termList.get(position);

        scoreAdapter = new ScoreAdapter(termList.get(position).getSubjects(), getApplicationContext());
        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(eLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(scoreAdapter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void doRefesh(View view) {
        fetchData();
    }

    private StringBuilder generateMessage(int pos) {
        TermList term = termList.get(pos);
        List<Subject> subjects = term.getSubjects();

        StringBuilder message = new StringBuilder(term.getName() + "\n\n");
        for (int i = 0; i < subjects.size(); i++) {
            message.append(i+1 + ". Môn "  + subjects.get(i).getName() + " xếp loại: " + subjects.get(i).getType() + "\n");
        }

        return message;
    }

    public void sendToHome(View view) {
        Information information = new Information();
        Gson gson = new Gson();
        String infoJson = mPrefs.getString("infoJson", "");
        information = gson.fromJson(infoJson, Information.class);

//        Toast.makeText(ScoreActivity.this, infoJson, Toast.LENGTH_SHORT).show();

        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", information.getNumberOfParent());

        StringBuilder message = generateMessage(pos);
        message.insert(0, "Gửi gia đình yêu quý điểm của con! \n\n");

        message.append("\nCon yêu gia đình nhiều lắm! ");

        smsIntent.putExtra("sms_body",message.toString());
        startActivity(smsIntent);
    }

    public void doShare(View view) {
        StringBuilder shareBody = generateMessage(pos);
        shareBody.insert(0, "Gửi bạn điểm: \n");
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Điểm của mình nè");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody.toString());
        startActivity(Intent.createChooser(sharingIntent, "Chia sẻ qua"));
    }
}
